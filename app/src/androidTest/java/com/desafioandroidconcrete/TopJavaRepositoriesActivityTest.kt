package com.desafioandroidconcrete

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import com.desafioandroidconcrete.ui.pullRequest.PullRequestActivity
import com.desafioandroidconcrete.ui.topJavaRepositories.TopJavaRepositoriesActivity
import com.desafioandroidconcrete.ui.topJavaRepositories.TopJavaRepositoriesViewHolder
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.allOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class TopJavaRepositoriesActivityTest {

    private var server = MockWebServer()

    @Rule
    @JvmField
    val mActivityRule= ActivityTestRule(TopJavaRepositoriesActivity::class.java, false, false)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        server.start()
        setupServerUrl()
    }

    private fun setupServerUrl() {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        server.url("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1")
    }

    @Test
    fun whenResultIsOkShouldDisplayListWithRepositories() {
        server.enqueue(MockResponse().setResponseCode(200))
        mActivityRule.launchActivity(Intent())
        onView(withId(R.id.pullRequestRecyclerView)).check(matches(isDisplayed()))
    }

    @Test
    fun checkUserItemViewIsDisplayed() {
        server.enqueue(MockResponse().setResponseCode(200))
        mActivityRule.launchActivity(Intent())
        onView(allOf<View>(withId(R.id.txtName), withText("elasticsearch"))).check(matches(isDisplayed()))
        onView(allOf<View>(withId(R.id.imgUser), hasSibling(withText("elastic")))).check(matches(isDisplayed()))
        onView(allOf<View>(withId(R.id.txtUsername), withText("elastic"))).check(matches(isDisplayed()))
    }

    @Test
    fun whenClickOnItemListShouldStartUserDetailsActivityWithExtra() {
        server.enqueue(MockResponse().setResponseCode(200))
        mActivityRule.launchActivity(Intent())
        Intents.init()

        val matcher = allOf<Intent>(
                hasComponent(PullRequestActivity::class.java.name),
                hasExtraWithKey("login"),
                hasExtraWithKey("name")
        )

        val result = Instrumentation.ActivityResult(Activity.RESULT_OK, null)
        intending(matcher).respondWith(result)
        onView(withId(R.id.pullRequestRecyclerView)).perform(
                actionOnItemAtPosition<TopJavaRepositoriesViewHolder>(0, click()))
        intended(matcher)
        Intents.release()
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        server.shutdown()
    }
}