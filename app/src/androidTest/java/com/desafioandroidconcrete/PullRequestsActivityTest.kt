package com.desafioandroidconcrete

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import com.desafioandroidconcrete.ui.pullRequest.PullRequestActivity
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers.allOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class PullRequestsActivityTest {

    private var server: MockWebServer? = null

    @Rule @JvmField
    val mActivityRule= ActivityTestRule(PullRequestActivity::class.java, false, false)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        server = MockWebServer()
        server!!.start()
        setupServerUrl()
    }

    private fun setupServerUrl() {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        server!!.url("https://api.github.com/repos/elastic/elasticsearch/pulls")
    }

    @Test
    fun whenResultIsOkShouldDisplayListWithRepositories() {
        server!!.enqueue(MockResponse().setResponseCode(200).setBody(Mock.SUCCESS_PULL))
        mActivityRule.launchActivity(createIntent())
        onView(withId(R.id.pullRequestRecyclerView)).check(matches(isDisplayed()))
    }

    private fun createIntent(): Intent {
        return Intent().putExtra("teste", "teste")
    }

    @Test
    fun checkUserItemViewIsDisplayed() {
        server!!.enqueue(MockResponse().setResponseCode(200).setBody(Mock.SUCCESS_PULL))
        mActivityRule.launchActivity(createIntent())
        onView(allOf<View>(withId(R.id.txtName), withText("jimczi/elasticsearch"))).check(matches(isDisplayed()))
        onView(allOf<View>(withId(R.id.imgUser), hasSibling(withText("jimczi")))).check(matches(isDisplayed()))
        onView(allOf<View>(withId(R.id.txtUsername), withText("jimczi"))).check(matches(isDisplayed()))
    }

    @After
    @Throws(IOException::class)
    fun tearDown() {
        server!!.shutdown()
    }
}