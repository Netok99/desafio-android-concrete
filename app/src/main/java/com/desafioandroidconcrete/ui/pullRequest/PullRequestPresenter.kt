package com.desafioandroidconcrete.ui.pullRequest

import android.util.Log
import com.desafioandroidconcrete.data.PullRequestDataSource
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class PullRequestPresenter(private val view: PullRequestContract.View,
                           private val dataSource: PullRequestDataSource) : PullRequestContract.Presenter {
    override fun getPullRequest(login: String, name: String) {
        view.showLoadingIndicator(true)

        dataSource.getPullRequest(login, name)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate {
                    view.showLoadingIndicator(false)
                }
                .subscribe({ pullRequests ->
                    view.setupRecyclerView(pullRequests)
                }, { error ->
                    Log.e("error", error.toString())
                })
    }
}
