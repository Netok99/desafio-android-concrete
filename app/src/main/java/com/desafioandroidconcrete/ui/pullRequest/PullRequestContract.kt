package com.desafioandroidconcrete.ui.pullRequest

import com.desafioandroidconcrete.data.model.PullRequest
import com.desafioandroidconcrete.ui.BaseView

interface PullRequestContract {

    interface View : BaseView {
        fun setupRecyclerView(pullRequests: List<PullRequest>)
    }

    interface Presenter {
        fun getPullRequest(login: String, name: String)
    }
}
