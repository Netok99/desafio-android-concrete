package com.desafioandroidconcrete.ui.pullRequest

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.desafioandroidconcrete.R
import com.desafioandroidconcrete.data.model.PullRequest
import com.desafioandroidconcrete.ui.BaseActivity
import com.desafioandroidconcrete.ui.PresenterModule
import com.desafioandroidconcrete.ui.UiComponent
import kotlinx.android.synthetic.main.activity_pull_request.*
import javax.inject.Inject

class PullRequestActivity : BaseActivity(), PullRequestContract.View {

    private lateinit var uiComponent: UiComponent
    private lateinit var login: String
    private lateinit var name: String
    private var pullRequests: List<PullRequest>? = null
    @Inject
    lateinit var presenter: PullRequestContract.Presenter
    private val keyParcelable = "pullRequests"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_request)
        uiComponent = getAppComponent() + PresenterModule(this)
        uiComponent.inject(this)

        login = intent.getStringExtra("login")
        name = intent.getStringExtra("name")

        setupUI()
        setupToolbar()
        if (savedInstanceState == null) {
            getPullRequest()
        } else {
            setupRecyclerView(savedInstanceState.getParcelableArrayList(keyParcelable))
        }
    }

    private fun setupToolbar() {
        toolbar.title = getString(R.string.title_pull_request_activity, name)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        if (pullRequests != null) {
            savedInstanceState.putParcelableArrayList(keyParcelable, pullRequests as ArrayList)
        }
    }

    private fun setupUI() = swipeRefresh.setOnRefreshListener({ getPullRequest() })

    private fun getPullRequest() = presenter.getPullRequest(login, name)

    override fun setupRecyclerView(pullRequests: List<PullRequest>) {
        this.pullRequests = pullRequests
        pullRequestRecyclerView.layoutManager = LinearLayoutManager(this)
        pullRequestRecyclerView.adapter = PullRequestAdapter(this, pullRequests)
    }

    override fun showLoadingIndicator(isLoading: Boolean) {
        swipeRefresh.isRefreshing = isLoading
    }
}
