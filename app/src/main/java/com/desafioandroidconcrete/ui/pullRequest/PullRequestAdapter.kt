package com.desafioandroidconcrete.ui.pullRequest

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.desafioandroidconcrete.R
import com.desafioandroidconcrete.data.model.PullRequest
import com.desafioandroidconcrete.ui.loadUrl
import kotlinx.android.synthetic.main.item_pull_request.view.*

class PullRequestAdapter(private val context: Context, private val pullRequests: List<PullRequest>):
        RecyclerView.Adapter<PullRequestViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PullRequestViewHolder
        = PullRequestViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_pull_request, parent, false))

    override fun onBindViewHolder(holder: PullRequestViewHolder, position: Int) =
            holder.bind(pullRequests[position])

    override fun getItemCount(): Int = pullRequests.size
}

class PullRequestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(pullRequest: PullRequest) = with(itemView) {
        val repo = pullRequest.head.repo
        if(repo?.full_name != null) txtName.text = repo.full_name
        if (pullRequest.body != null) txtBody.text = pullRequest.body
        if(repo?.owner?.avatar_url != null) imgUser.loadUrl(repo.owner.avatar_url)
        txtUsername.text = repo?.owner?.login

        setOnClickListener {
            try {
                context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(repo?.html_url)))
            } catch (e: Exception) {
                Toast.makeText(context, context.getString(R.string.error_html_url), Toast.LENGTH_LONG).show()
            }
        }
    }
}
