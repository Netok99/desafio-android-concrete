package com.desafioandroidconcrete.ui.topJavaRepositories

import android.util.Log
import com.desafioandroidconcrete.data.StarJavaRepositoriesDataSource
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class TopJavaRepositoriesPresenter(private val view: TopJavaRepositoriesContract.View,
                                   private val dataSource: StarJavaRepositoriesDataSource) : TopJavaRepositoriesContract.Presenter {
    override fun getTopRepositoriesJava(page: Int) {
        view.showLoadingIndicator(true)

        dataSource.getStarJavaRepositories(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate {
                    view.showLoadingIndicator(false)
                }
                .subscribe({ repository ->
                    view.setupRecyclerView(repository)
                }, { error ->
                    Log.e("error", error.toString())
                })
    }
}
