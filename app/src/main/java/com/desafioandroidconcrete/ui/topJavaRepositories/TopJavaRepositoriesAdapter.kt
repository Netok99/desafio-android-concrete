package com.desafioandroidconcrete.ui.topJavaRepositories

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.desafioandroidconcrete.R
import com.desafioandroidconcrete.data.model.Item
import com.desafioandroidconcrete.data.model.Repository
import com.desafioandroidconcrete.ui.loadUrl
import com.desafioandroidconcrete.ui.pullRequest.PullRequestActivity
import kotlinx.android.synthetic.main.item_top_repositories.view.*

class TopJavaRepositoriesAdapter(private val context: Context): RecyclerView.Adapter<TopJavaRepositoriesViewHolder>() {

    private var items: MutableList<Item> = mutableListOf()

    fun addItem(repository: Repository) {
        for (item in repository.items) {
            items.add(item)
            notifyItemInserted(items.size - 1)
        }
    }

    fun clearItems() {
        val size = items.size
        items.clear()
        notifyItemRangeRemoved(0, size)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TopJavaRepositoriesViewHolder
        = TopJavaRepositoriesViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.item_top_repositories, parent, false))

    override fun onBindViewHolder(holder: TopJavaRepositoriesViewHolder, position: Int) =
            holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int) = position

    override fun getItemId(position: Int): Long = position.toLong()
}

class TopJavaRepositoriesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: Item) = with(itemView) {
        txtName.text = item.name
        txtDescription.text = item.description
        txtNumberForks.text = item.forks_count.toString()
        txtNumberStars.text = item.stargazers_count.toString()
        imgUser.loadUrl(item.owner.avatar_url)
        txtUsername.text = item.owner.login

        setOnClickListener {
            val intent = Intent(context, PullRequestActivity::class.java)
            intent.putExtra("login", item.owner.login)
            intent.putExtra("name", item.name)
            context.startActivity(intent)
        }
    }
}
