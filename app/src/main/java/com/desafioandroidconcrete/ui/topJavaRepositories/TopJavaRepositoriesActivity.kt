package com.desafioandroidconcrete.ui.topJavaRepositories

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.WindowManager
import com.desafioandroidconcrete.R
import com.desafioandroidconcrete.data.model.Repository
import com.desafioandroidconcrete.ui.BaseActivity
import com.desafioandroidconcrete.ui.EndlessRecyclerOnScrollListener
import com.desafioandroidconcrete.ui.PresenterModule
import com.desafioandroidconcrete.ui.UiComponent
import kotlinx.android.synthetic.main.activity_top_repositories_java.*
import javax.inject.Inject

class TopJavaRepositoriesActivity : BaseActivity(), TopJavaRepositoriesContract.View {

    private lateinit var uiComponent: UiComponent
    private lateinit var adapter: TopJavaRepositoriesAdapter
    @Inject
    lateinit var presenter: TopJavaRepositoriesContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_repositories_java)
        uiComponent = getAppComponent() + PresenterModule(this)
        uiComponent.inject(this)

        setupToolbar()
        setupUI()
        getTopRepositoriesJava(1)
    }

    private fun setupToolbar() {
        toolbar.title = getString(R.string.top_java_repositories)
        setSupportActionBar(toolbar)
    }

    private fun setupUI() {
        swipeRefresh.setOnRefreshListener({
            adapter.clearItems()
            getTopRepositoriesJava(1)
            setupTopJavaRepositoriesList()
        })
        setupTopJavaRepositoriesList()
    }

    private fun setupTopJavaRepositoriesList() {
        val layoutManager = LinearLayoutManager(this)
        topJavaRepositoriesRecyclerView.layoutManager = layoutManager
        topJavaRepositoriesRecyclerView.addOnScrollListener(EndlessRecyclerOnScrollListener(layoutManager,
                { getTopRepositoriesJava(it) }))
        adapter = TopJavaRepositoriesAdapter(this)
        topJavaRepositoriesRecyclerView.adapter = adapter
        topJavaRepositoriesRecyclerView.setHasFixedSize(true)
        topJavaRepositoriesRecyclerView.setItemViewCacheSize(20)
        topJavaRepositoriesRecyclerView.isDrawingCacheEnabled = true
        topJavaRepositoriesRecyclerView.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
    }

    private fun getTopRepositoriesJava(page: Int) = presenter.getTopRepositoriesJava(page)

    override fun setupRecyclerView(repository: Repository) = adapter.addItem(repository)

    override fun showLoadingIndicator(isLoading: Boolean) {
        swipeRefresh.isRefreshing = isLoading
    }
}
