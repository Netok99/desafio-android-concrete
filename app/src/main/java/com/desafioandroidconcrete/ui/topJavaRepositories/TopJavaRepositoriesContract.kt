package com.desafioandroidconcrete.ui.topJavaRepositories

import com.desafioandroidconcrete.data.model.Repository
import com.desafioandroidconcrete.ui.BaseView


interface TopJavaRepositoriesContract {

    interface View : BaseView {
        fun setupRecyclerView(repository: Repository)
    }

    interface Presenter {
        fun getTopRepositoriesJava(page: Int)
    }
}
