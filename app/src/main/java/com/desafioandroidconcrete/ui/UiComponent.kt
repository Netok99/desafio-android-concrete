package com.desafioandroidconcrete.ui

import com.desafioandroidconcrete.data.PullRequestDataSource
import com.desafioandroidconcrete.data.StarJavaRepositoriesDataSource
import com.desafioandroidconcrete.ui.pullRequest.PullRequestActivity
import com.desafioandroidconcrete.ui.pullRequest.PullRequestContract
import com.desafioandroidconcrete.ui.pullRequest.PullRequestPresenter
import com.desafioandroidconcrete.ui.topJavaRepositories.TopJavaRepositoriesActivity
import com.desafioandroidconcrete.ui.topJavaRepositories.TopJavaRepositoriesContract
import com.desafioandroidconcrete.ui.topJavaRepositories.TopJavaRepositoriesPresenter
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(PresenterModule::class))
interface UiComponent {
    fun inject(activity: TopJavaRepositoriesActivity)
    fun inject(activity: PullRequestActivity)
}

@Module
open class PresenterModule(private val view: BaseView) {

    @Provides
    fun provideTopJavaRepositoriesPresenter(dataSource: StarJavaRepositoriesDataSource):
            TopJavaRepositoriesContract.Presenter {
        if (view !is TopJavaRepositoriesContract.View) {
            throw ClassCastException("view should be TopJavaRepositoriesContract.View")
        }
        return TopJavaRepositoriesPresenter(view, dataSource)
    }

    @Provides
    fun providePullRequestPresenter(dataSource: PullRequestDataSource): PullRequestContract.Presenter {
        if (view !is PullRequestContract.View) {
            throw ClassCastException("view should be PullRequestContract.View")
        }
        return PullRequestPresenter(view, dataSource)
    }
}
