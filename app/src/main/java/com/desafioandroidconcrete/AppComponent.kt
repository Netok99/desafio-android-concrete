package com.desafioandroidconcrete

import android.content.Context
import com.desafioandroidconcrete.data.APIModule
import com.desafioandroidconcrete.data.DataModule
import com.desafioandroidconcrete.ui.PresenterModule
import com.desafioandroidconcrete.ui.UiComponent
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, DataModule::class, APIModule::class))
interface AppComponent {
    fun inject(app: App)
    operator fun plus(presenterModule: PresenterModule): UiComponent
}

@Module
class AppModule(val app: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideApp(): App = app
}
