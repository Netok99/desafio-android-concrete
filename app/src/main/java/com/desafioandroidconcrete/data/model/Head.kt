package com.desafioandroidconcrete.data.model

import android.os.Parcel
import android.os.Parcelable

data class Head(val repo: Repo?) : Parcelable {
    constructor(source: Parcel) : this(
            source.readParcelable<Repo>(Repo::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(repo, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Head> = object : Parcelable.Creator<Head> {
            override fun createFromParcel(source: Parcel): Head = Head(source)
            override fun newArray(size: Int): Array<Head?> = arrayOfNulls(size)
        }
    }
}
