package com.desafioandroidconcrete.data.model

data class Item(val id: Int, val name: String, val full_name: String, val description: String,
                val forks_count: Int, val stargazers_count: Int, val owner: Owner)
