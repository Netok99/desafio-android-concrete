package com.desafioandroidconcrete.data.model

import android.os.Parcel
import android.os.Parcelable

data class Repo(val full_name: String?, val description: String?, val avatar_url: String?,
                val login: String, val html_url: String, val owner: Owner) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readParcelable<Owner>(Owner::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(full_name)
        writeString(description)
        writeString(avatar_url)
        writeString(login)
        writeString(html_url)
        writeParcelable(owner, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Repo> = object : Parcelable.Creator<Repo> {
            override fun createFromParcel(source: Parcel): Repo = Repo(source)
            override fun newArray(size: Int): Array<Repo?> = arrayOfNulls(size)
        }
    }
}

