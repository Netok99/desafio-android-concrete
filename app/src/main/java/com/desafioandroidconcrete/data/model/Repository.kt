package com.desafioandroidconcrete.data.model

import android.os.Parcel
import android.os.Parcelable

data class Repository(val total_count: Int, val incomplete_results: Boolean, val items: List<Item>) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            1 == source.readInt(),
            ArrayList<Item>().apply { source.readList(this, Item::class.java.classLoader) }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(total_count)
        writeInt((if (incomplete_results) 1 else 0))
        writeList(items)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Repository> = object : Parcelable.Creator<Repository> {
            override fun createFromParcel(source: Parcel): Repository = Repository(source)
            override fun newArray(size: Int): Array<Repository?> = arrayOfNulls(size)
        }
    }
}
