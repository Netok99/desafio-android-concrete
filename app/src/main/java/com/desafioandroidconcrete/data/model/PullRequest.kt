package com.desafioandroidconcrete.data.model

import android.os.Parcel
import android.os.Parcelable

data class PullRequest(val head: Head, val body: String?) : Parcelable {
    constructor(source: Parcel) : this(
            source.readParcelable<Head>(Head::class.java.classLoader),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(head, 0)
        writeString(body)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<PullRequest> = object : Parcelable.Creator<PullRequest> {
            override fun createFromParcel(source: Parcel): PullRequest = PullRequest(source)
            override fun newArray(size: Int): Array<PullRequest?> = arrayOfNulls(size)
        }
    }
}

