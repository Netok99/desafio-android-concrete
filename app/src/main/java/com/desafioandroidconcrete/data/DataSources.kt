package com.desafioandroidconcrete.data

import com.desafioandroidconcrete.data.model.PullRequest
import com.desafioandroidconcrete.data.model.Repository
import rx.Observable

interface StarJavaRepositoriesDataSource {
    fun getStarJavaRepositories(page: Int): Observable<Repository>
}

interface PullRequestDataSource {
    fun getPullRequest(login: String, name: String): Observable<List<PullRequest>>
}
