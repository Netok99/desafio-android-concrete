package com.desafioandroidconcrete.data.remote

import com.desafioandroidconcrete.data.model.PullRequest
import com.desafioandroidconcrete.data.model.Repository
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

interface APIService {

    @GET("search/repositories?q=language:Java&sort=stars")
    fun getTopJavaRepositories(@Query("page") page: Int): Observable<Repository>

    @GET("repos/{repository}/{creator}/pulls")
    fun getPullRequest(@Path("repository") repository: String, @Path("creator") creator: String):
            Observable<List<PullRequest>>
}
