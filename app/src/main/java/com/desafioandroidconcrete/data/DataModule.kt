package com.desafioandroidconcrete.data

import com.desafioandroidconcrete.App
import com.desafioandroidconcrete.data.remote.APIService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideStarJavaRepositoriesRepository(api: APIService, app: App):
            StarJavaRepositoriesDataSource = StarJavaRepositoriesRepository(api, app)

    @Provides
    @Singleton
    fun providePullRequestRepository(api: APIService, app: App):
            PullRequestDataSource = PullRequestDataRepository(api, app)
}
