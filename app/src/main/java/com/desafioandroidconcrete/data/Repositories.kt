package com.desafioandroidconcrete.data

import com.desafioandroidconcrete.App
import com.desafioandroidconcrete.data.model.PullRequest
import com.desafioandroidconcrete.data.model.Repository
import com.desafioandroidconcrete.data.remote.APIService
import rx.Observable

class StarJavaRepositoriesRepository(private val api: APIService,
                                     private val app: App) : StarJavaRepositoriesDataSource {
    override fun getStarJavaRepositories(page: Int): Observable<Repository> = api.getTopJavaRepositories(page)
}

class PullRequestDataRepository(private val api: APIService,
                                private val app: App) : PullRequestDataSource {
    override fun getPullRequest(login: String, name: String): Observable<List<PullRequest>> =
            api.getPullRequest(login, name)
}
