package com.desafioandroidconcrete

import com.desafioandroidconcrete.data.PullRequestDataSource
import com.desafioandroidconcrete.data.model.Head
import com.desafioandroidconcrete.data.model.Owner
import com.desafioandroidconcrete.data.model.PullRequest
import com.desafioandroidconcrete.data.model.Repo
import com.desafioandroidconcrete.ui.pullRequest.PullRequestContract
import com.desafioandroidconcrete.ui.pullRequest.PullRequestPresenter
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import rx.lang.kotlin.toSingletonObservable

class PullRequestPresenterTest {

    @Rule
    @JvmField
    val rxField = RxSchedulersOverrideRule()

    private lateinit var view: PullRequestContract.View
    private lateinit var presenter: PullRequestContract.Presenter
    private lateinit var dataSource: PullRequestDataSource

    @Before
    fun setup() {
        view = mock()
        dataSource = mock()
        presenter = PullRequestPresenter(view, dataSource)
    }

    private val pullRequestList = listOf(
            PullRequest(Head(Repo("teste", "description test", "url test",
                    "neto", "http://ww.google.com", Owner("avatar url test", "neto")))),
            PullRequest(Head(Repo("teste2", "description test2", "url test2",
                    "neto2", "http://ww.google.com", Owner("avatar url test2", "neto2"))))
    )

    @Test
    fun shouldGetAndPullRequestList() {
        val login = "ReactiveX"
        val name = "RxJava"

        Mockito.`when`(dataSource.getPullRequest(login, name))
                .thenReturn(pullRequestList.toSingletonObservable())

        presenter.getPullRequest(login, name)

        Mockito.verify(view).showLoadingIndicator(true)
        Mockito.verify(dataSource).getPullRequest(login, name)
        Mockito.verify(view).setupRecyclerView(pullRequestList)
        Mockito.verify(view).showLoadingIndicator(false)
        Mockito.verifyNoMoreInteractions(view, dataSource)
    }
}