package com.desafioandroidconcrete

import com.desafioandroidconcrete.data.StarJavaRepositoriesDataSource
import com.desafioandroidconcrete.data.model.*
import com.desafioandroidconcrete.ui.topJavaRepositories.TopJavaRepositoriesContract
import com.desafioandroidconcrete.ui.topJavaRepositories.TopJavaRepositoriesPresenter
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import rx.lang.kotlin.toSingletonObservable

class TopJavaRepositoriesPresenterTest {

    @Rule
    @JvmField
    val rxField = RxSchedulersOverrideRule()

    private lateinit var view: TopJavaRepositoriesContract.View
    private lateinit var presenter: TopJavaRepositoriesContract.Presenter
    private lateinit var dataSource: StarJavaRepositoriesDataSource

    @Before
    fun setup() {
        view = mock()
        dataSource = mock()
        presenter = TopJavaRepositoriesPresenter(view, dataSource)
    }

    private val repository = Repository(10, true, listOf(
            Item(1, "teste", "complete name", "description", 10,
                    50, Owner("avatar url", "owner"))
    ))

    @Test
    fun shouldGetAndPullRequestList() {
        val page = 1

        Mockito.`when`(dataSource.getStarJavaRepositories(1))
                .thenReturn(repository.toSingletonObservable())

        presenter.getTopRepositoriesJava(page)

        Mockito.verify(view).showLoadingIndicator(true)
        Mockito.verify(dataSource).getStarJavaRepositories(page)
        Mockito.verify(view).setupRecyclerView(repository)
        Mockito.verify(view).showLoadingIndicator(false)
        Mockito.verifyNoMoreInteractions(view, dataSource)
    }
}